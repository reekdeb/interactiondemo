﻿using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Handle events.
/// </summary>
[RequireComponent(typeof(EventTrigger))]
public class Interaction : MonoBehaviour
{
    [SerializeField]
    private LayerMask layerMask = -1;

    private Manager manager;
    private Animator uiAnimator;
    private EventTrigger eventTrigger;
    private Camera mainCamera;

    private int animParamUIState = Animator.StringToHash("UIState");

    private bool canDrag = true;
    private Vector3 targetPosition;

    private void Awake()
    {
        manager = FindObjectOfType<Manager>();
        uiAnimator = GameObject.Find("CanvasControls").GetComponent<Animator>();
        eventTrigger = GetComponent<EventTrigger>();
        mainCamera = Camera.main;
    }

    private void Start()
    {
        // Setup event handlers
        EventTrigger.Entry dragEntry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Drag
        };
        dragEntry.callback.AddListener((data) => { OnDrag((PointerEventData)data); });
        eventTrigger.triggers.Add(dragEntry);
        var dragBeginEntry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.BeginDrag
        };
        dragBeginEntry.callback.AddListener((data) => OnDragBegin((PointerEventData)data));
        eventTrigger.triggers.Add(dragBeginEntry);
        var dragEndEntry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.EndDrag
        };
        dragEndEntry.callback.AddListener((data) => OnDragEnd((PointerEventData)data));
        eventTrigger.triggers.Add(dragEndEntry);
    }

    private void OnTriggerEnter(Collider other)
    {
        canDrag = false;
    }

    /// <summary>
    /// Hide UI OnDragBegin.
    /// </summary>
    /// <param name="eventData">Pointer event data.</param>
    private void OnDragBegin(PointerEventData eventData)
    {
        uiAnimator.SetBool(animParamUIState, false);
    }

    /// <summary>
    /// Show UI OnDragEnd.
    /// </summary>
    /// <param name="eventData">Pointer event data.</param>
    private void OnDragEnd(PointerEventData eventData)
    {
        canDrag = true;
        uiAnimator.SetBool(animParamUIState, true);
    }

    /// <summary>
    /// Handle OnDrag event. Translate or rotate OnDrag.
    /// </summary>
    /// <param name="eventData">Pointer event data.</param>
    private void OnDrag(PointerEventData eventData)
    {
        if (!canDrag) return;
        // Perform translate or rotate based on current mode while dragging
        switch (manager.CurrentMode)
        {
            case TransformModes.Translate:
                Translate(eventData.position);
                break;
            case TransformModes.Rotate:
                Rotate(eventData);
                break;
        }
    }

    /// <summary>
    /// Translate target.
    /// </summary>
    /// <param name="screenPos">Screen position in pixels.</param>
    private void Translate(Vector3 screenPos)
    {
        var ray = mainCamera.ScreenPointToRay(screenPos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask.value))
        {
            transform.position = hit.point;
        }
    }

    /// <summary>
    /// Rotate target.
    /// </summary>
    /// <param name="eventData">Pointer event data.</param>
    private void Rotate(PointerEventData eventData)
    {
        transform.Rotate(0.0f, 0.0f, -eventData.delta.x);
    }
}
