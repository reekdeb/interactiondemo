﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Game Manager.
/// </summary>
public class Manager : MonoBehaviour
{
    [SerializeField]
    private Interaction[] targets;
    [SerializeField]
    private Transform lookAt;
    [SerializeField]
    private Button replaceButton;

    private int curentGo = 0;

    private TransformModes mMode = TransformModes.Translate;

    /// <summary>
    /// Current transform mode.
    /// </summary>
    public TransformModes CurrentMode { get { return mMode; } }


    private void OnEnable()
    {
        // Setup replace button OnClick listener
        replaceButton.onClick.AddListener(() => ReplaceTargetGo());
    }


    private void Start()
    {
        // Setup scene
        for (int i = 0; i < targets.Length; i++)
            if (i == 0) targets[i].gameObject.SetActive(true);
            else targets[i].gameObject.SetActive(false);
        lookAt.SetParent(targets[curentGo].transform, false);
    }

    /// <summary>
    /// Set current transform mode.
    /// </summary>
    /// <param name="mode">Transform mode.</param>
    public void SetMode(int mode) { mMode = (TransformModes)mode; }

    private void ReplaceTargetGo()
    {
        // Get current position and rotation
        var t = targets[curentGo].transform;
        var pos = t.position;
        var rot = t.rotation.eulerAngles;

        // Detach LookAt from parent
        lookAt.SetParent(null, false);

        // Disable current target
        targets[curentGo].gameObject.SetActive(false);

        // Enable next target
        ++curentGo;
        if (curentGo < targets.Length)
            targets[curentGo].gameObject.SetActive(true);
        else
        {
            curentGo = 0;
            targets[curentGo].gameObject.SetActive(true);
        }

        // Set position and rotaion of new target
        targets[curentGo].transform.position = pos;
        targets[curentGo].transform.rotation = Quaternion.Euler(rot);

        // Set LookAt as child of current target
        lookAt.SetParent(targets[curentGo].transform, false);
    }
}
