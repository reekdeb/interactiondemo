﻿/// <summary>
/// Transform Modes.
/// </summary>
public enum TransformModes
{
    /// <summary>
    /// Translate mode.
    /// </summary>
    Translate,
    /// <summary>
    /// Rotate mode.
    /// </summary>
    Rotate
}
